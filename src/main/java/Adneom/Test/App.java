package Adneom.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

/**
 * App Class !
 * @author Laurel TALOM
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Test Java Adneom !" );
    }

    /**
     * Use this function to obtain a list of many partitions.
     * 
     * @param liste list of numbers
     * @param taille length of each partition
     * @return A list of sub lists of partition
     */
	public static List<List<Integer>> partition(Integer[] liste, int taille) {
		
	    List<Integer> intList = Lists.newArrayList(liste);
		
		Map<Integer, List<Integer>> groups = intList.stream()
													.collect(Collectors.groupingBy(s -> (s - 1) / taille));
		
	    List<List<Integer>> subSets = new ArrayList<List<Integer>>(groups.values());
		
		return subSets;
	}
}
