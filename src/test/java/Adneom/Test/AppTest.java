package Adneom.Test;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.google.common.collect.Lists;
import java.util.List;

/**
 * Unit test for App.
 */
public class AppTest extends TestCase
{
	
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Test of the partition function
     */
    public void testAppPartition_taille_2()
    {
    	Integer[] numbers = {1,2,3,4,5};
    	int taille = 2;
    	
    	List<List<Integer>> subSets = App.partition(numbers, taille);

        List<Integer> expectedFirstPartition = Lists.<Integer> newArrayList(1, 2);
        List<Integer> expectedMiddlePartition = Lists.<Integer> newArrayList(3, 4);
        List<Integer> expectedLastPartition = Lists.<Integer> newArrayList(5);

        assertEquals(2, subSets.get(0).size());
        assertSame(expectedFirstPartition.get(0), subSets.get(0).get(0));
        assertSame(expectedMiddlePartition.get(1), subSets.get(1).get(1));

        assertEquals(2, subSets.get(1).size());
        assertSame(expectedMiddlePartition.get(0), subSets.get(1).get(0));
        assertSame(expectedMiddlePartition.get(1), subSets.get(1).get(1));
        
        assertEquals(1, subSets.get(2).size());
        assertSame(expectedLastPartition.get(0), subSets.get(2).get(0));
        
        assertTrue( true );
    }
}
