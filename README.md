# TestJava

Test JAVA Instructions

0. Rassurez-vous d'avoir intallé Java (Jre et JDK 1.8 minimum) et Maven (site officiel : http://maven.apache.org/download.cgi) sur votre machine.

1. Cloner le projet depuis le repo distant et importer le projet dans un IDE (Eclipse par exemple) en tant que "Maven Project".

2. Vérifier bien d’avoir installé Java 8 (JavaSE-1 8) ou plus sur votre machine et de l’utiliser dans le projet.

3. Exécuter l’application avec JUnit 4$: Clic droit sur le projet > Run As > JUnit Test .


4. Enjoy ! :)
